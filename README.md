redux.vim
=========

A readable dark colorscheme for [Vim](http://www.vim.org/).

This started out as murphy, and then was rewritten, with some influence from [solarized](https://github.com/altercation/vim-colors-solarized).


Installation
------------

Add the following lines to your `.vimrc` or `.config/nvim/init.vim`, depending on your desired method of installation...


### Using [vim-plug](https://github.com/junegunn/vim-plug) ###

```vim
" In your plugins section...
Plug 'https://gitlab.com/skewed-aspect/redux.vim.git'

" Later in the file...
colorscheme redux
```


### Using [NeoBundle](https://github.com/Shougo/neobundle.vim) ###

```vim
" In your plugins section...
NeoBundle 'https://gitlab.com/skewed-aspect/redux.vim.git'

" Later in the file...
colorscheme redux
```


### Using [Vundle](https://github.com/gmarik/vundle) ###

```vim
" In your plugins section...
Plugin 'https://gitlab.com/skewed-aspect/redux.vim.git'

" Later in the file...
colorscheme redux
```


### Using [Pathogen](https://github.com/tpope/vim-pathogen) ###

Clone this repository into `~/.vim/bundle/` or `~/.config/nvim/bundle/`, and then add this to your `.vimrc` or
`.config/nvim/init.vim`:

```vim
colorscheme redux
```


### Manual Installation ###

Download `colors/redux.vim` and put it somewhere in your Vim `runtimepath` (e.g., `~/.vim/colors/redux.vim` or
`~/.config/nvim/colors/redux.vim`) and then add this to your `.vimrc` or `.config/nvim/init.vim`:

```vim
colorscheme redux
```

" vim: et tw=0 ts=4 sts=4 sw=4 colorcolumn=21,38,60
" Vim color file
"
" Maintainer:   Chris Case <chris.case@g33xnexus.com>
" Maintainer:   David H. Bronke <whitelynx@gmail.com>
" Last Change:  2015 January 28

hi clear
set background=dark
if exists("syntax_on")
    syntax reset
endif

let g:colors_name = "redux"


"===== Monochrome Terminals =====

hi Comment           term=bold
hi String            term=underline
hi Constant          term=underline
hi Identifier        term=underline
hi PreProc           term=underline
hi Search            term=reverse
hi Special           term=bold
hi Statement         term=bold
hi Error             term=reverse
hi Todo              term=standout

hi Directory         term=bold
hi IncSearch         term=reverse
hi LineNr            term=underline
hi ModeMsg           term=bold
hi MoreMsg           term=bold
hi NonText           term=bold
hi SpecialKey        term=bold
hi Visual            term=reverse

hi Title             term=bold
hi Question          term=standout
hi WarningMsg        term=standout
hi ErrorMsg          term=standout

hi StatusLine        term=reverse,bold
hi StatusLineNC      term=reverse

hi TabLine           term=underline
hi TabLineSel        term=bold
hi TabLineFill       term=reverse


"===== Color Terminals =====

hi Normal                             ctermfg=LightGrey     ctermbg=Black
hi Comment                            ctermfg=DarkGrey
hi String                             ctermfg=LightCyan
hi Constant                           ctermfg=LightBlue
hi Identifier                         ctermfg=Yellow
hi Ignore                             ctermfg=black
hi PreProc                            ctermfg=DarkYellow
hi Special                            ctermfg=LightGreen
hi Statement                          ctermfg=DarkYellow
hi Type                               ctermfg=Magenta
hi Error                              ctermfg=White         ctermbg=Red
hi Todo                               ctermfg=Black         ctermbg=Yellow

hi Directory                          ctermfg=LightCyan
hi IncSearch         cterm=reverse
hi LineNr                             ctermfg=Yellow
hi ModeMsg           cterm=bold
hi MoreMsg                            ctermfg=LightGreen
hi NonText                            ctermfg=Blue
hi SpecialKey                         ctermfg=LightBlue
hi Visual            cterm=reverse    ctermfg=DarkGrey

hi Title                              ctermfg=LightMagenta
hi Question                           ctermfg=LightGreen
hi WarningMsg                         ctermfg=LightRed
hi ErrorMsg                           ctermfg=White         ctermbg=DarkRed

hi StatusLine        cterm=reverse
hi StatusLineNC      cterm=reverse
hi VertSplit         cterm=reverse

hi CursorColumn                                             ctermbg=DarkGrey
hi ColorColumn                                              ctermbg=DarkGrey
hi CursorLine                                               ctermbg=NONE

hi TabLine                            ctermfg=Grey          ctermbg=DarkGrey
hi TabLineSel        cterm=bold       ctermfg=White         ctermbg=DarkGrey
hi TabLineFill                        ctermfg=bg            ctermbg=DarkGrey

hi Pmenu                              ctermfg=Black         ctermbg=DarkGrey
hi PmenuSel                           ctermfg=Black         ctermbg=Grey
hi PmenuSbar                                                ctermbg=Grey
hi PmenuThumb                                               ctermbg=White

"== Folds ==
hi Folded            cterm=none       ctermfg=Cyan          ctermbg=DarkGrey

hi FoldColumn        cterm=none       ctermfg=Cyan          ctermbg=DarkGrey

"== Diffs ==
hi DiffDelete        cterm=none       ctermfg=Red           ctermbg=DarkRed
hi DiffAdd           cterm=none       ctermfg=Green         ctermbg=DarkGreen
hi DiffChange        cterm=none       ctermfg=fg            ctermbg=Brown
hi DiffText          cterm=bold       ctermfg=Yellow        ctermbg=Brown

hi diffOldFile                        ctermfg=Red
"hi diffNewFile                        ctermfg=Green
" FIXME: The "diff" syntax seems to use completely the wrong colors for the
" filename header lines; the old file (denoted by "---") is highlighted with
" "diffNewFile", and the new file (denoted by "+++") is highlighted with
" diffFile". The diff command line, if present, is highlighted with "diffFile"
" as well, and the "index" line (from "git diff") is not highlighted.
" The highlighting below is a HACK to make it display _somewhat_ correctly:
hi diffNewFile                        ctermfg=Red
hi diffFile                           ctermfg=Green

"== Signs ==
hi SignColumn        cterm=none       ctermfg=117           ctermbg=233


if &t_Co==256
    " 256color terminal support here
    hi Normal                         ctermfg=15
    hi Type                           ctermfg=141
    hi String                         ctermfg=80
    hi Constant                       ctermfg=69
    hi Identifier                     ctermfg=172
    hi PreProc                        ctermfg=179
    hi Statement                      ctermfg=179
    hi Special                        ctermfg=77

    hi Visual        cterm=NONE       ctermfg=NONE          ctermbg=238

    hi StatusLine    cterm=NONE,bold  ctermfg=255           ctermbg=238
    hi StatusLineNC  cterm=NONE,bold  ctermfg=242           ctermbg=235
    hi VertSplit                      ctermfg=101           ctermbg=237

    "TODO: Double-check this!
    hi TabLine                        ctermfg=244           ctermbg=235
    hi TabLineSel                     ctermfg=153           ctermbg=235
    hi TabLineFill                    ctermfg=187           ctermbg=235

    hi CursorColumn                                         ctermbg=232
    hi ColorColumn                                          ctermbg=233

    hi CursorLine    cterm=NONE                             ctermbg=234

    hi Pmenu                          ctermfg=0             ctermbg=239
    hi PmenuSel                       ctermfg=0             ctermbg=235
    hi PmenuSbar                                            ctermbg=236
    hi PmenuThumb                                           ctermbg=243

    hi TabLine                        ctermfg=244           ctermbg=236
    hi TabLineSel    cterm=bold       ctermfg=254           ctermbg=236
    hi TabLineFill                    ctermfg=187           ctermbg=236

    "== Folds ==
    hi Folded        cterm=none       ctermfg=117           ctermbg=235

    hi FoldColumn    cterm=none       ctermfg=117           ctermbg=232

    "== Diffs ==
    hi DiffDelete    cterm=none       ctermfg=217           ctermbg=52
    hi DiffAdd       cterm=none       ctermfg=157           ctermbg=22
    hi DiffChange    cterm=none       ctermfg=NONE          ctermbg=58
    hi DiffText      cterm=bold       ctermfg=221           ctermbg=58

    hi diffOldFile   cterm=underline  ctermfg=210
    "hi diffNewFile   cterm=underline  ctermfg=120
    " FIXME: The "diff" syntax seems to use completely the wrong colors for the
    " filename header lines; the old file (denoted by "---") is highlighted
    " with "diffNewFile", and the new file (denoted by "+++") is highlighted
    " with "diffFile". The diff command line, if present, is highlighted with
    " "diffFile" as well, and the "index" line (from "git diff") is not
    " highlighted.
    " The highlighting below is a HACK to make it display _somewhat_ correctly:
    hi diffNewFile   cterm=underline  ctermfg=132
    hi diffFile      cterm=underline  ctermfg=111

    "== Signs ==
    hi SignColumn    cterm=none       ctermfg=117           ctermbg=233
endif

" Link colors for the "diff" file format to the "diff mode" colors.
" (consistency!)
hi link diffRemoved DiffDelete
hi link diffAdded DiffAdd
hi link diffChanged DiffText


"===== GUI =====

hi Normal                             guifg=#F5EAD5         guibg=#000000
hi Comment                            guifg=#777777
hi String            gui=NONE         guifg=#6AD3EB
hi Constant          gui=NONE         guifg=#7598FF
hi Identifier                         guifg=#D78700
hi Ignore                             guifg=bg
hi PreProc                            guifg=#684F2B
hi Search                             guifg=#ffff55         guibg=#555555
hi Special                            guifg=#7ED980
hi Statement         gui=NONE         guifg=#D4A259
hi Type              gui=none         guifg=#B391E6
hi Error                              guifg=#FFFFFF         guibg=#FF0000
hi Todo                               guifg=#0000FF         guibg=#FFFF00

hi Cursor                             guifg=#da70d6         guibg=fg
hi Directory                          guifg=#00FFFF
hi IncSearch         gui=reverse
hi LineNr                             guifg=#FFFF00
hi ModeMsg           gui=bold
hi MoreMsg           gui=bold         guifg=#2E8B57
hi NonText           gui=bold         guifg=#0000FF
hi SpecialKey                         guifg=#00FFFF
hi Visual            gui=NONE         guifg=NONE            guibg=#444444

hi Title             gui=bold         guifg=#FFC0CB
hi Question          gui=bold         guifg=#00FFFF
hi WarningMsg                         guifg=#FF0000
hi ErrorMsg                           guifg=#FFFFFF         guibg=#FF0000

hi StatusLine        gui=NONE         guifg=#e0e0e0         guibg=#414658
hi StatusLineNC      gui=NONE         guifg=#767986         guibg=#292C36
hi VertSplit                          guifg=#777777         guibg=#363946

hi CursorColumn                                             guibg=#0a0a0a
hi ColorColumn                                              guibg=#0f0f0f
hi CursorLine                                               guibg=#1a1a1a

hi TabLine                            guifg=#b6bf98         guibg=#363946
hi TabLineSel        gui=bold         guifg=#efefef         guibg=#414658
hi TabLineFill                        guifg=#cfcfaf         guibg=#363946

hi Pmenu                                                    guibg=#525252
hi PmenuSel                                                 guibg=#2e2e2e
hi PmenuSbar                                                guibg=#333333
hi PmenuThumb                                               guibg=#7a7a7a

"== Folds ==
hi Folded            gui=none         guifg=#91d6f8         guibg=#292C36

hi FoldColumn        gui=none         guifg=#91d6f8         guibg=#1F2129

"== Diffs ==
"FIXME: Right now, these are copied directly from luciusblack, and they don't really fit.
hi DiffDelete        gui=none         guifg=#f05060         guibg=#4a343a
hi DiffAdd           gui=none         guifg=#80a090         guibg=#313c36
hi DiffChange        gui=none         guifg=NONE            guibg=#3f3b21
hi DiffText          gui=bold         guifg=#9f9c58         guibg=#3f3b21
    hi DiffDelete    cterm=none       ctermfg=217           ctermbg=52
    hi DiffAdd       cterm=none       ctermfg=157           ctermbg=22
    hi DiffChange    cterm=none       ctermfg=NONE          ctermbg=58
    hi DiffText      cterm=bold       ctermfg=221           ctermbg=58

"== Signs ==
hi SignColumn        gui=none         guifg=#91d6f8         guibg=#181818
